**Steps to start the web service to get the 'dummy json' response for Hemang:**

* Clone this repo:
    - `git clone https://shubhamshukla7@bitbucket.org/shubhamshukla7/hackathon.git`
    
* cd into the repo just cloned:
    - `cd hackathon/`
    
* Create a virtual env with pip (you might need to install pip if you don't already have it):
    - `python3 -m virtualenv my_env`
    
* Activate the venv:
    - `. ./my_env/bin/activate`

* Install the requirements file into this virtual env:
    - `pip3 install -r requirements.txt`

* Start the service:
    - `python services/web_service.py`


**Service Doc:**

* Base URL: http://127.0.0.1:5000
* Endpoint: /company/details
* Method: POST
* Request parameter: JSON that looks like this: `{"company_name": [COMPANY_NAME]}`
* Response: JSON data to be used by the "company detail" webpage

**Sample Example using cURL**

* Request:

```python
      curl -X POST \
      http://127.0.0.1:5000/company/details \
      -H 'cache-control: no-cache' \
      -H 'postman-token: 457a5405-b050-4595-7f2c-b41953b1a76d' \
      -d '{
        "company_name": "cabs.com"
      }
```
      
* Response:

```json
        {
            "competitors": [
                {
                    "id": 2,
                    "name": "Lyft LC",
                    "url": "www.lyft.com"
                },
                {
                    "id": 3,
                    "name": "Uber.com LLC",
                    "url": "www.uber.com"
                },
                {
                    "id": 3,
                    "name": "Wheels LLC",
                    "url": "www.wheel.com"
                },
                {
                    "id": 3,
                    "name": "GoGoGo LLC",
                    "url": "www.gogogo.com"
                }
            ],
            "id": 1,
            "name": "cabs.com",
            "recent_press_coverage": [
                {
                    "title": "cabs.com CEO resigns",
                    "url": "www.google.com"
                },
                {
                    "title": "Is cabs.com the new Lyft?",
                    "url": "www.netflix.com"
                },
                {
                    "title": "cabs.com sees some growth in 4th quarter",
                    "url": "www.youtube.com"
                }
            ],
            "scores": [
                {
                    "score": 81,
                    "type": "Management Team"
                },
                {
                    "score": 74,
                    "type": "Financials"
                },
                {
                    "score": 45,
                    "type": "Social Sentiment"
                }
            ]
        }
```

NY Times Search API:
- https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=0e723c6fad1a458893ee55e23403af45&q=lyft&&fq=news_desk:(%22Business%20Day%22%20%22Business%22%20%22Entrepreneurs%22%20%22Financial%22%20%22Foreign%22%20%22Personal%20Investing%22%20%22Personal%20Tech%22%20%22Politics%22%20%22Regionals%22%20%22Retail%22%20%22Science%22%20%22Small%20Business%22%20%22Sunday%20Business%22%20%22Technology%22%20%22Your%20Money%22)

API Keys:
- 0e723c6fad1a458893ee55e23403af45

SQL Queries:
- select * from cb_objects where first_funding_at is NOT NULL and entity_type = 'Company' and status <> 'closed' and country_code = 'USA' and overview is NOT NULL and overview <> '' and city is NOT NULL and state_code is NOT NULL and description is NOT NULL and short_description is NOT NULL and homepage_url is NOT NULL and category_code in ('games_video', 'medical', 'travel','finance','mobile','software','messaging','music','fashion','search','transportation','mobile','NULL','ecommerce');

category_code = games_video or medical or travel or finance or mobile or software 
or messaging or music or fashion search or transportation or mobile or NULL or ecommerce

amazon: ecommerce
uber/lyft: NULL, transportation, 
+------------------+
| category_code    |
+------------------+
| news             |
| advertising      |
| finance          |
| games_video      |
| ecommerce        |
| web              |
| medical          |
| network_hosting  |
| real_estate      |
| software         |
| search           |
| enterprise       |
| mobile           |
| music            |
| photo_video      |
| hardware         |
| fashion          |
| nonprofit        |
| cleantech        |
| analytics        |
| messaging        |
| biotech          |
| social           |
| security         |
| education        |
| semiconductor    |
| health           |
| consulting       |
| travel           |
| other            |
| design           |
| public_relations |
| nanotech         |
| automotive       |
| NULL             |
| manufacturing    |
| sports           |
| hospitality      |
| transportation   |
| legal            |
| government       |
| pets             |
| local            |
+------------------+
43 rows in set (55.70 sec)

Ecommerce:
- select entity_id, name, homepage_url from cb_objects where name in ('Costco', 'Target', 'eBay', 'Flipkart', 'Amazon', 'Alibaba', 'Jd.com', 'Walmart', 'Walmart eCommerce');
- add jet manually

Ride share:
- select * from cb_objects where name in ('Uber', 'Lyft', )
Yellow Taxi Cab California - Mountain View
Zipcar

gaming_sports

Medical

Travel

Music

Fashion

