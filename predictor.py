from topic_identifier import preprocess_text, _merge_text
from sklearn.metrics.pairwise import euclidean_distances
from util.http_client import get_latest_news
import pickle
import os


def most_similar(company, model, vectorizer, Z, index_to_company_dict, use_wsj = False, top_n=5):
    overview = preprocess_text(company['overview'])

    result = {}
    for model in [model]:
        x = model.transform(vectorizer.transform([_merge_text(company)]))[0]
        dists = euclidean_distances(x.reshape(1, -1), Z)
        pairs = enumerate(dists[0])
        most_similar = sorted(pairs, key=lambda item: item[1])[:top_n]

        entry = []
        for each_similar in most_similar:
            company_to_append = index_to_company_dict[each_similar[0]]
            if use_wsj:
                company_to_append['news'] = get_latest_news(company_to_append['name'])
            else:
                company_to_append['news'] = []

            entry.append(company_to_append)

        result["competitors"] = entry
        result["id"] = company['id']
        result["name"] = company['name']
        result["scores"] = company['scores']
        result["recent_press_coverage"] = company['recent_press_coverage']

    return result



#
# company = {
#         "overview": "Founded in August 2008 and based in San Francisco, California, Airbnb is a trusted community marketplace for people to list, discover, and book unique vacation rentals around the world -- online or from a mobile phone. Whether an apartment for a night, a castle for a week, or a house for a month, Airbnb connects people to unique travel and vacation experiences, at any price point, in more than 26,000 cities and 192 countries. And with world-class customer service and a growing community of users, Airbnb is the easiest way for people to monetize their extra space and showcase it to an audience of millions.",
#         "name": "Airbnb",
#     }
#
# infile = open("{}/model.final.pkl".format(this_dir),'rb')
# result = pickle.load(infile)
# infile.close()
#
# res = most_similar(company, result['model'], result['vectorizer'], result['Z'], result['index_to_company_dict'], use_wsj=False, top_n=3)
# for name in res['type']:
#     print(name['name'])
