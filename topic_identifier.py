from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import euclidean_distances
from util.vectorizer import get_count_vectorizer, get_tfidf_vectorizer
from util.data_preprocessor import preprocess_text
from sklearn.decomposition import NMF, LatentDirichletAllocation
from util.http_client import get_latest_news
import numpy as np
import pickle
import json
import os

CLUSTER_COUNT = 3
N_TOP_WORDS = 15
vectorizer = get_tfidf_vectorizer()  # get_count_vectorizer()


this_dir = os.path.dirname(os.path.abspath(__file__))

def _merge_text(company):
    return preprocess_text(company['overview'])

def create_models(companies_json, cluster_count):
    companies = []
    index_to_company = {}
    idx = 0
    for company in companies_json:
        companies.append(_merge_text(company))
        index_to_company[idx] = company
        print("reading company {}/3633".format(idx))
        idx += 1


    X = vectorizer.fit_transform(companies)

    vocab = vectorizer.get_feature_names()
    print("vocab: " + str(vocab))
    print("len(vocab): " + str(len(vocab)))
    model = LatentDirichletAllocation(n_topics=CLUSTER_COUNT, random_state=200)
    id_topic = model.fit_transform(X)

    result = {}
    for x in range(cluster_count):
        result.setdefault(x, [])

    idx = 0
    for cluster_id in id_topic.argmax(axis=1):
        result[cluster_id].append(index_to_company[idx]['name'])
        idx += 1

    topic_words = {}
    for topic, comp in enumerate(model.components_):
        word_idx = np.argsort(comp)[::-1][:N_TOP_WORDS]

        # store the words most relevant to the topic
        topic_words[topic] = [vocab[i] for i in word_idx]

    # for topic, words in topic_words.items():
    #     print('Topic: %d' % topic)
    #     print('  %s' % ', '.join(words))

    result = {
        'model': model,
        'vectorizer': vectorizer,
        'Z': id_topic,
        'index_to_company_dict': index_to_company
    }

    outfile = open("{}/model.final.pkl".format(this_dir), 'wb')
    pickle.dump(result, outfile)
    outfile.close()

    print("training completed successfully!!")
    return result


# train the algorithm...
if __name__ == '__main__':
    companies_json_file = "{}/dummy_data/3000_startups.json.golden".format(this_dir)
    with open(companies_json_file) as data_file:
        companies_json = json.load(data_file)
    create_models(companies_json, CLUSTER_COUNT)