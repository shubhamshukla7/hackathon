from flask import Flask, render_template, request, jsonify
import requests
import json


app = Flask(__name__)

CORE_SERVICE_URL = 'http://localhost:5000/company/details'

@app.route("/")
@app.route("/home")
def home():
	return render_template('home.html')

@app.route("/lookup", methods=["POST"])
def do_lookup():
	if request.method == "POST":
		# get the datadata
		data = request.get_json(force="true")
		#jsonify(json.load(request.get_json(force=True)))
		
		response = call_core_service(data)
		print(response)

		# print the data
	#return render_template('home.html')
	return render_template('company.html', data=data)


def call_core_service(data):
	headers = {'content-type': 'text/json'}
	print("CORE_SERVICE_URL: " + CORE_SERVICE_URL)
	print("data: " + str(data))

	response = requests.post(CORE_SERVICE_URL, data=data, headers=headers)	
	response = response
	return response

if __name__ == '__main__':
    app.run(port='1337')