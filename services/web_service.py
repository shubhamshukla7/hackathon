from flask import Flask, request, jsonify
from predictor import most_similar
import json
import os
import pickle
import nltk


app = Flask(__name__)

this_dir = os.path.dirname(os.path.abspath(__file__))

infile = open("{}/../model.final.pkl".format(this_dir),'rb')
result = pickle.load(infile)
infile.close()


use_wsj = False
top_n = 4


@app.route("/company/details", methods=["POST"])
def get_company_details():

    company_name = request.get_json(force=True)['company_name']
    company = get_company_info(company_name)

    print ("Company name: {}".format(company_name))

    # res = most_similar(company, result['model'], result['vectorizer'], result['Z'], result['index_to_company_dict'],
    #                    use_wsj=use_wsj, top_n=top_n)

    return _return_dummy_json()


def get_company_info(company_name):
    company =     {
        "description": "Online Real Estate ",
        "overview": "Redfin is an online real estate company that provides real estate search and brokerage services. Redfin uses an interesting combination of online real estate search and access to live agents. They employ their agents so they can better control customer satisfaction; traditional brokerage firms license their name to independent agents. Redfin claims to save homebuyers on average $7,500 by reimbursing roughly 1/2 of the buy-side real estate fee directly on closing. Redfin also pays bonuses to agents when they receive high customer satisfaction. The service launched in February 2006.",
        "name": "Redfin",
        "founded_at": "2004-10-01",
        "id": 195,
        "short_description": "Redfin provides real estate search and brokerage services through a combination of real estate web platforms and access to live agents.",
        "city": "Seattle",
        "logo_url": "http://s3.amazonaws.com/crunchbase_prod_assets/assets/images/resized/0020/4948/204948v1-max-250x250.png",
        "funding_rounds": 7,
        "funding_total_usd": "96820000",
        "url": "http://redfin.com",
        "category_code": "real_estate",
        "scores": [
          {
            "type": "Management Team",
            "score": 69
          },
          {
            "type": "Financials",
            "score": 87
          },
          {
            "type": "Social Sentiment",
            "score": 71
          }
        ],
        "recent_press_coverage": [
          {
            "title": "London's Black Cabs Soon to Be Seen on the Streets of Paris",
            "link": "https://www.nytimes.com/aponline/2018/10/18/world/europe/ap-eu-britain-france-black-cabs.html"
          },
          {
            "title": "Where Yellow Cabs Didn’t Go, Green Cabs Were Supposed to Thrive. Then Came Uber.",
            "link": "https://www.nytimes.com/2018/09/03/nyregion/green-cabs-yellow-uber.html"
          },
          {
            "title": "A City Grapples With Ride Hailing Apps",
            "link": "https://www.nytimes.com/2018/08/05/opinion/letters/ride-hailing.html"
          }
        ]
      }
    return company



def _return_dummy_json():
    this_dir = os.path.dirname(os.path.realpath(__file__))
    json_file_path = "{}/../dummy_data/dummy_response.json".format(this_dir)

    with open(json_file_path) as json_data:
        response = json.load(json_data)
    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True, port='5001')
