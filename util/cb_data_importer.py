import MySQLdb
import json
import os

SQL_QUERY = "select * from cb_objects where first_funding_at is NOT NULL and entity_type = 'Company' and status <> 'closed' and country_code = 'USA' and overview is NOT NULL and overview <> '' and city is NOT NULL and state_code is NOT NULL and description is NOT NULL and short_description is NOT NULL and homepage_url is NOT NULL and category_code in ('games_video', 'medical', 'travel','finance','mobile','software','messaging','music','fashion','search','transportation','mobile','NULL','ecommerce');"
SQL_QUERY = "select * from cb_objects where first_funding_at is NOT NULL and entity_type = 'Company' and status <> 'closed' and country_code = 'USA' and overview is NOT NULL and overview <> '' and city is NOT NULL and state_code is NOT NULL and description is NOT NULL and homepage_url is NOT NULL and category_code in ('fashion', 'transportation', 'travel', 'music', 'automotive', 'real_estate') order by name;"
SQL_QUERY = "select * from cb_objects where id in ('c:12', 'c:179637', 'c:5', 'r:199')"
hostname = 'localhost'
username = 'root'
password = 'admin'
database = 'hackathon'


this_dir = os.path.dirname(os.path.abspath(__file__))

def load_cb_data(host, user, passwd, db, output_file="{}/../dummy_data/3000_startups.json.golden".format(this_dir)):
    connection = MySQLdb.connect(host, user, passwd, db)
    cursor = connection.cursor()
    cursor.execute(SQL_QUERY)
    results = cursor.fetchall()

    response = []
    counter = 0
    for result in results:
        print("Loading entry #{}/3698".format(counter))
        counter += 1

        entry = {}

        id = result[2]
        name = result[4]
        category = result[7]
        url = result[12]
        short_description = result[17]
        description = result[18]
        overview = result[19]
        logo_url = result[14]
        founded_at = result[9]
        city = result[23]
        funding_rounds = result[31]
        funding_total_usd = result[32]

        entry['id'] = id
        entry['name'] = name
        entry['category_code'] = category
        entry['url'] = url
        entry['short_description'] = short_description
        entry['description'] = description
        entry['overview'] = overview
        entry['logo_url'] = logo_url
        entry['founded_at'] = str(founded_at)
        entry['city'] = city
        entry['funding_rounds'] = funding_rounds
        entry['funding_total_usd'] = str(funding_total_usd)
        response.append(entry)

    with open(output_file, 'w') as outfile:
        json.dump(response, outfile)

    print(results)


load_cb_data(hostname, username, password, database)

# print "Using pymysql…"
# import pymysql
# myConnection = pymysql.connect( host=hostname, user=username, passwd=password, db=database )
# doQuery( myConnection )
# myConnection.close()
#
# print "Using mysql.connector…"
# import mysql.connector
# myConnection = mysql.connector.connect( host=hostname, user=username, passwd=password, db=database )
# doQuery( myConnection )
# myConnection.close()
#
