import nltk
from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
#
# nltk.download('punkt')

stop_words = set(stopwords.words('english'))
stop_words.add("'")
stop_words.add(".")
stop_words.add(",")
stop_words.add("!")
stop_words.add("#")
stemmer = PorterStemmer()

uber = ["car", "cars", "service", "ride", "driver", "friendly", "safe", "transportation",
              "mobile", "app", "web", "experience", "members", "membership", "member", "places",
              "place", "friend", "vehicles", "vehicle", "share", "sharing", "travelers", "traveling",
              "travel", "safety", "friend", "rides", "cab", "cabs", "technology", "driver", "drivers"]
airbnb = ["vacation", "vacations", "activity", "activities", "lodging", "rental", "rentals",
          "planning", "rent", "renting", "apartment", "apt", "apartments", "book", "booking",
          "bookings", "adventure", "plan", "family", "friends", "friend", "adventures",
          "search", "listings", "find", "search", "ideas", "checkin", "checkin", "check-in",
          "home", "homes", "application", "homeowners", "property", "managers", "properties",
          "homeowner", "home", "hotel", "hotels", "stay", "renters"]
redfin = ["real", "estate", "sale", "sales", "agent", "agents", "sellers", "sale,", "saller",
          "sales", "buy", "buyer", "buyers", "buying", "tenants", "tenant", "commercial", "residential",
          "residence", "listing", "online", "property", "properties", "rental", "renters", "neighborhood",
          "house", "home", "hunting", "brokerage", "firms", "broker"]
# social_networking = ["social", "networking", "messaging", "message", "messages", "friends", "friend",
#                      "connection", "connections", "active", "users", "user", "communicate", "communications",
#                      "visitors", "world", "network", "user", "visit", "visits",
#                      "visitor", "pictures", "picture", "photo", "photos"]


def _get_preprocessed_dictionary():

    preprocessed_dictionary = set()
    preprocessed_dictionary |= set(uber)
    preprocessed_dictionary |= set(airbnb)
    preprocessed_dictionary |= set(redfin)
    preprocessed_dictionary |= set(social_networking)
    for word in preprocessed_dictionary:
        word = word.lower()
        word = stemmer.stem(word)
        preprocessed_dictionary.add(word)
    return preprocessed_dictionary



def preprocess_text_new(verbose):
    # to lower
    verbose = verbose.lower()

    # get tokens
    verbose = word_tokenize(verbose)

    # remove stop words
    stop_words_removed = []
    for w in verbose:
        if w not in stop_words:
            stop_words_removed.append(w)

    stemmed_with_stop_words_removed = ""
    for w in stop_words_removed:
        w = stemmer.stem(w)
        if w in _get_preprocessed_dictionary():
            stemmed_with_stop_words_removed += w + " "

    return stemmed_with_stop_words_removed


def preprocess_text(verbose):

    # to lower
    verbose = verbose.lower()

    # get tokens
    verbose = word_tokenize(verbose)

    # remove stop words
    stop_words_removed = []
    for w in verbose:
        if w not in stop_words:
            stop_words_removed.append(w)

    # stem words
    result = ""
    for w in stop_words_removed:
        result += stemmer.stem(w) + " "

    return result

