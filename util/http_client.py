import requests
import time
import json

NYT_API_KEY = "0e723c6fad1a458893ee55e23403af45"

# nyt_url_format = "https://api.nytimes.com/svc/search/v2/articlesearch.json?" \
#       "api-key={}" \
#       "&fq=news_desk:" \
#       "(\"Business Day\" " \
#       "\"Business\" " \
#       "\"Entrepreneurs\" " \
#       "\"Financial\" " \
#       "\"Foreign\" " \
#       "\"Personal Investing\" " \
#       "\"Personal Tech\" " \
#       "\"Politics\" " \
#       "\"Regionals\" " \
#       "\"Retail\" " \
#       "\"Science\" " \
#       "\"Small Business\" " \
#       "\"Sunday Business\" " \
#       "\"Technology\" " \
#       "\"Your Money\")".format(NYT_API_KEY)

nyt_url_format = "https://api.nytimes.com/svc/search/v2/articlesearch.json?" \
      "api-key={}" \
      "&fq=news_desk:" \
      "(\"Business Day\" " \
      "\"Business\" " \
      "\"Entrepreneurs\" " \
      "\"Financial\" " \
      "\"Personal Investing\" " \
      "\"Personal Tech\" " \
      "\"Science\" " \
      "\"Small Business\" " \
      "\"Sunday Business\" " \
      "\"Technology\" " \
      "\"Your Money\")".format(NYT_API_KEY)

def get_latest_news(q):
      url = "{}&q={}".format(nyt_url_format, q)
      response = requests.get(url)
      time.sleep(1.0)
      raw_articles = json.loads(response.text)['response']['docs']

      news_articles = []
      for raw_article in raw_articles:
            polished_article = {}
            polished_article['url'] = raw_article['web_url']
            polished_article['headline'] = raw_article['headline']['print_headline']
            polished_article['snippet'] = raw_article['snippet']
            news_articles.append(polished_article)
      return news_articles