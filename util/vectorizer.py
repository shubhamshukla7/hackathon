from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

def get_count_vectorizer():
    # crete model and serialize it
    # count_vectorizer = CountVectorizer(min_df=0.2, max_df=0.7,
    #                                    stop_words='english', lowercase=True,
    #                                    token_pattern='[a-zA-Z\-][a-zA-Z\-]{2,}')
    count_vectorizer = CountVectorizer(max_df=0.30)
    return count_vectorizer

def get_tfidf_vectorizer():
    vectorizer = TfidfVectorizer(min_df=2, max_df=0.99, stop_words='english')
    vectorizer = CountVectorizer(min_df=0.2, max_df=0.7,
                                       stop_words='english', lowercase=True,
                                       token_pattern='[a-zA-Z\-][a-zA-Z\-]{2,}')

    return vectorizer


